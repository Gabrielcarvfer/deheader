#
# makefile for `deheader'
#
INSTALL=install
prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

VERS=$(shell sed <deheader -n -e '/version\s*=\s*"\(.*\)"/s//\1/p')

SOURCES = README COPYING NEWS.adoc deheader deheader.xml Makefile control deheader-logo.png test

all: deheader.1

deheader.1: deheader.xml
	xmlto man deheader.xml

deheader.html: deheader.xml
	xmlto html-nochunks deheader.xml

INSTALLABLES := deheader
MANPAGES := deheader.1

install:
	$(INSTALL) -d "$(target)/bin"
	$(INSTALL) -d "$(target)/share/doc/deheader"
	$(INSTALL) -d "$(target)/$(mandir)/man1"
	$(INSTALL) -m 755 $(INSTALLABLES) "$(target)/bin"
	$(INSTALL) -m 644 $(MANPAGES) "$(target)/$(mandir)/man1"

INSTALLED_BINARIES := $(INSTALLABLES:%="$(target)/bin/%")
INSTALLED_MANPAGES := $(MANPAGES:%="$(target)/$(mandir)/man1/%")

uninstall:
	rm -f $(INSTALLED_BINARIES)
	rm -f $(INSTALLED_MANPAGES)
	rmdir "$(target)/share/doc/deheader"

clean:
	rm -f *~ *.1 *.html test/*.o test/*~ MANIFEST

check:
	cd test; make --quiet check
checkfile:
	cd test; make --quiet checkfile

pylint:
	@pylint --score=n deheader

version:
	@echo $(VERS)

deheader-$(VERS).tar.gz: $(SOURCES) deheader.1
	tar --transform='s:^:deheader-$(VERS)/:' --show-transformed-names -cvzf deheader-$(VERS).tar.gz $(SOURCES) deheader.1

dist: deheader-$(VERS).tar.gz

release: deheader-$(VERS).tar.gz deheader.html
	shipper version=$(VERS) | sh -e -x

refresh: deheader.html
	shipper -N -w version=$(VERS) | sh -e -x
